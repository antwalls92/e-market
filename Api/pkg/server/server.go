package server

import (
	"../api"
	"../data"
	"github.com/gorilla/mux"
	"net/http"
)

type Server interface {
	Router() http.Handler
}

func NewServer() Server {
	//investigate dependency injection
	r := mux.NewRouter()
	invoiceRepository := data.InvoiceRepository{}

	return api.NewInvoiceApi(&invoiceRepository,r)

}




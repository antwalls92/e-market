package api

import (
	"../data"
	"encoding/json"
	"github.com/gorilla/mux"
	"net/http"
)

type InvoiceApi struct {
	Repository data.IInvoiceRepository
	router *mux.Router
}

func NewInvoiceApi(repository data.IInvoiceRepository, router *mux.Router) *InvoiceApi {
	api := &InvoiceApi{Repository: repository, router: router}
	api.ConfigureRoutes()
	return api
}


func (a *InvoiceApi) FetchInvoices(w http.ResponseWriter, request *http.Request) {
	invoices, _ := a.Repository.FetchInvoices()

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(invoices)
}

func (a *InvoiceApi) FetchInvoice(writer http.ResponseWriter, request *http.Request) {
    vars := mux.Vars(request)
    invoice, err := a.Repository.FetchInvoiceByID(vars["ID"])
	writer.Header().Set("Content-Type", "application/json")
    if err != nil {
    	writer.WriteHeader(http.StatusNotFound)
    	json.NewEncoder(writer).Encode("Invoice not found")
		return
    }

    json.NewEncoder(writer).Encode(invoice)
}


func (a * InvoiceApi) ConfigureRoutes() {
	a.router.HandleFunc("/invoices", a.FetchInvoices).Methods(http.MethodGet)
	a.router.HandleFunc("/invoices/{ID:[a-zA-Z0-9_]+}", a.FetchInvoice).Methods(http.MethodGet)
}

func (a *InvoiceApi) Router() http.Handler {
	return a.router
}
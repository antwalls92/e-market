package dtos

import "encoding/json"

type Invoice struct {
	Id   string `json:"ID"`
	Date string `json:"date,omniempty`
	Price json.Number
	Concept string
	Name string
}


package data

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"../dtos"
)

type IInvoiceRepository interface {
	FetchInvoices() ([] dtos.Invoice, error)
	FetchInvoiceByID(s string) (dtos.Invoice, error)
}

type InvoiceRepository struct{

}

func (repostory *InvoiceRepository) FetchInvoices() ([] dtos.Invoice, error) {

	path, err := os.Getwd()
	if err != nil {
		log.Println(err)
	}
	fmt.Println(path)
	//this should be moved to a new repository
		jsonFile, err := os.Open("invoices.json")
		if err != nil{
			fmt.Println(err)
		}
		defer jsonFile.Close()
	//
	byteValue, _ := ioutil.ReadAll(jsonFile)
	var invoices []dtos.Invoice

	json.Unmarshal(byteValue, &invoices)

	return invoices,err
}

func (repostory *InvoiceRepository) FetchInvoiceByID(id string) (dtos.Invoice, error) {

	invoices, err := repostory.FetchInvoices()
	for _, n := range invoices {
		if n.Id == id{
			return n, err
		}
	}
	return dtos.Invoice{}, err
}






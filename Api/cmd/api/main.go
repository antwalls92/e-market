package main

import (
	"../../pkg/server"
	"log"
	"net/http"
)

func main() {
	s := server.NewServer()
	log.Fatal(http.ListenAndServe(":8080", s.Router()))
}